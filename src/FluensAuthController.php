<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use GuzzleHttp;

class FluensAuthController extends Controller
{
    /**
     * Attempts to log in a user using the FluensAuth OAuth server.
     * If none of this seems to work, make sure you have set the
     * FLUENSAUTH_* variables in the .env file (see .env.example)
     *
     * @author Dennis @date 04/07/2018
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function attemptLogin(Request $request){
        if ((!$request->has('email')) || (!$request->has('password'))) {
            return back()->withErrors(["message" => "Gelieve uw e-mailadres en wachtwoord in te vullen."]);
        }
        if(count(User::where('email',$request['email'])->get())>0){
            if($this->checkCredentials($request) > 0){
                $u = User::where('email',$request['email'])->first();
                if($u->expires_at < Carbon::now()->addHours(10)){
                    $response = $this->getPasswordGrantToken($request);
                    $u->access_token = $response["access_token"];
                    $u->refresh_token = $response["refresh_token"];
                    $u->expires_at = Carbon::now()->addSeconds($response["expires_in"]);
                    $u->save();
                }
                Auth::login($u);
                return redirect('/');
            }else{
                return back()->withErrors(['message' => 'Het e-mailadres of het wachtwoord is onjuist. Probeer het opnieuw']);
            }
        }

        try{
            $response = $this->getPasswordGrantToken($request);
        }catch(GuzzleHttp\Exception\ClientException $e){
            return back()->withErrors(['message' => 'Het e-mailadres of het wachtwoord is onjuist. Probeer het opnieuw, of maak een account aan indien u er nog geen heeft.']);
        }
        $headers = ['Authorization' => 'Bearer ' . $response["access_token"]];

        $user = $this->getFluensAuthUser($request,$headers);

        $lUser = new User($user);
        $lUser->access_token = $response["access_token"];
        $lUser->refresh_token = $response["refresh_token"];
        $lUser->expires_at = Carbon::now()->addSeconds($response["expires_in"]);
        $lUser->save();
        Auth::login($lUser);
        return redirect('/');
    }

    /**
     * Fetches the Password Grant token from the FluensAuth server,
     * and returns the response body from the server
     *
     * @author Dennis @date 04/07/2018
     * @param Request $request
     * @return String
     */
    private function getPasswordGrantToken(Request $request)
    {
        $http = new GuzzleHttp\Client;
        $response = $http->post(env('FLUENSAUTH_URL') . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => env('FLUENSAUTH_ID'),
                'client_secret' => env('FLUENSAUTH_SECRET'),
                'username' => $request->get('email'),
                'password' => $request->get('password'),
                'scope' => '*',
            ],
        ]);

        return json_decode((string) $response->getBody(), true);
    }

    /**
     * Retrieves the user data for a newly made user
     *
     * @author Dennis @date 04/07/2018
     * @param Request $request
     * @param array $headers
     * @return mixed
     */
    private function getFluensAuthUser(Request $request, array $headers)
    {
        $http = new GuzzleHttp\Client;
        $response = $http->get(env('FLUENSAUTH_URL').'/api/user',[
            'headers' => $headers
        ]);
        return json_decode((string) $response->getBody(), true);
    }

    /**
     * Checks with the FluensAuth server to make sure the given credentials
     * are correct, for logging in returning users.
     *
     * @author Dennis @date 04/07/2018
     * @param Request $request
     * @return integer 1 if successful, -1 if email unknown, -2 if password incorrect
     */
    private function checkCredentials(Request $request)
    {
        $http = new GuzzleHttp\Client;
        $response = $http->post(env('FLUENSAUTH_URL').'/api/credentials',[
            'form_params' => [
                'email' => $request->get('email'),
                'password' => $request->get('password')
            ]
        ]);
        return json_decode((string) $response->getBody(), true);
    }

    /**
     * Attempts to register the user with the FluensAuth server
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(Request $request){
        $http = new GuzzleHttp\Client;
        $response = $http->post(env('FLUENSAUTH_URL') . '/register', [
            'form_params' => [
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => $request->get('password'),
                'password_confirmation' => $request->get('password_confirmation'),
            ]
        ]);

        //controleren of account succesvol is aangemaakt
        $reply = $this->checkCredentials($request);
        switch($reply){
            case 1:
                $message = 'Uw account werd succesvol aangemaakt. U kan nu inloggen';
                break;
            case -2:
                $message = 'Er bestaat reeds een account met dit e-mailadres';
                break;
            default:
                $message = 'Er ging iets mis. Probeer het later opnieuw';
                break;
        }
        return redirect('/')->with('message',$message);
    }
}
